/**
 * @file
 * Graphs sourcer for HTML table Javascript library.
 */

(function ($) {

Drupal.behaviors.graphs_s_htmltable = {
  attach: function (context, settings) { },

  detach: function (context, settings) { },

  /**
   * Initializes the graph data by parsing the HTML table.
   *
   * Drupal.settings.graphs.sourcers[sourcer_id].nodes is a hash (object) of all
   * graph nodes keyed by ids.
   * Drupal.settings.graphs.sourcers[sourcer_id].graph is an oject that can be
   * processed by many D3JS built-in functions. It has 2 keys, 'nodes' which is
   * an array of node objects (with an 'id' property) and 'links' which is an
   * array of link objects (with a 'source', and a 'target' properties).
   */
  init: function (sourcer_id) {

    var sourcer = Drupal.settings.graphs.sourcers[sourcer_id];
    var nodes = sourcer.nodes;
    var graph = {
      'nodes': [],
      'links': [],
    };

    // Make sure settings are correct and adjust values.
    // Column settings are 1-based and we need 0-based indexes so we decrease
    // column values by 1 when set.
    if (!sourcer.id_column || (1 > sourcer.id_column)) {
      console.log('Invalid data sourcer settings: id column number is negative!');
      sourcer.id_column = 0;
    }
    else {
      --sourcer.id_column;
    }

    var columns = [
      'child_link_column',
      'class_column',
      'length_column',
      'name_column',
      'other_link_column',
      'other_link_type_column',
      'parent_link_column',
      'tooltip_column'
    ];

    columns.forEach(function (item) {
      if (0 < sourcer[item]) {
        --sourcer[item];
      }
      else {
        sourcer[item] = false;
      }
    });

    $(sourcer.table_selector + ' >tbody >tr').each(function (index, row) {
      var node_id = $(row).find('td:eq(' + sourcer.id_column + ')').text().trim();
      // Node must have an ID.
      if ('' != node_id) {
        // Only add nodes once and ignore duplicates (they might be used to
        // specify multiple links).
        if (!(node_id in nodes)) {
          nodes[node_id] = {
            'id': node_id,
            'relationships': {'child_of': [], 'parent_of': []}
          };
          graph.nodes.push(nodes[node_id]);
        }
        else {
          // Warn duplicates.
          console.log("Duplicate entry found for node id '" + node_id + "'");
        }

        // Name.
        if (false !== sourcer.name_column) {
          var name = $(row)
            .find('td:eq(' + sourcer.name_column + ')')
            .text()
            .trim();
          if ('' != name) {
            nodes[node_id].name = name;
          }
        }
        else {
          if (node_id.match(/^\d+$/)) {
            nodes[node_id].name = 'node ' + node_id;
          }
          else {
            // Capitalize first letter.
            nodes[node_id].name = node_id.charAt(0).toUpperCase() + node_id.slice(1);
          }
        }

        // Tooltip.
        if (false !== sourcer.tooltip_column) {
          var tooltip = $(row)
            .find('td:eq(' + sourcer.tooltip_column + ')')
            .text()
            .trim();
          if ('' != tooltip) {
            nodes[node_id].tooltip = tooltip;
          }
        }

        // Class.
        if (false !== sourcer.class_column) {
          var classes = $(row)
            .find('td:eq(' + sourcer.class_column + ')')
            .text()
            .trim();
          if ('' != classes) {
            nodes[node_id].classes = classes;
          }
        }

        // Links.
        var branch_length = 1;
        if (false !== sourcer.length_column) {
          var length_value = $(row)
            .find('td:eq(' + sourcer.length_column + ')')
            .text()
            .trim();
          if (length_value && !isNaN(length_value)) {
            branch_length = +length_value;
          }
        }

        if (false !== sourcer.parent_link_column) {
          var parent_ids = $(row)
            .find('td:eq(' + sourcer.parent_link_column + ')')
            .text()
            .split(',');
          $.each(parent_ids, function (index, parent_id) {
            parent_id = parent_id.trim();
            if ('' != parent_id) {
              var relationship = {
                'source': node_id,
                'target': parent_id,
                'relationship': 'child_of',
                'branch_length': branch_length
              };
              nodes[node_id].relationships['child_of'].push(relationship);
              graph.links.push(relationship);
            }
          });
        }

        if (false !== sourcer.child_link_column) {
          var child_ids = $(row)
            .find('td:eq(' + sourcer.child_link_column + ')')
            .text()
            .split(',');
          $.each(child_ids, function (index, child_id) {
            child_id = child_id.trim();
            if ('' != child_id) {
              var relationship = {
                'source': node_id,
                'target': child_id,
                'relationship': 'parent_of',
                'branch_length': branch_length
              };
              nodes[node_id].relationships['parent_of'].push(relationship);
              graph.links.push(relationship);
            }
          });
        }

        if ((false !== sourcer.other_link_column)
            && (false !== sourcer.other_link_type_column)) {
          var link_type = $(row)
            .find('td:eq(' + sourcer.other_link_type_column + ')')
            .text()
            .trim();
          if ('' != link_type) {
            var linked_ids = $(row)
              .find('td:eq(' + sourcer.other_link_column + ')')
              .text()
              .split(',');
            $.each(linked_ids, function (index, linked_id) {
              linked_id = linked_id.trim();
              if ('' != linked_id) {
                var relationship = {
                  'source': node_id,
                  'target': linked_id,
                  'relationship': link_type,
                  'branch_length': branch_length
                };
                if (!nodes[node_id].relationships[link_type]) {
                  nodes[node_id].relationships[link_type] = [];
                }
                nodes[node_id].relationships[link_type].push(relationship);
                graph.links.push(relationship);
              }
            });
          }
        }

      }

    });
    sourcer.graph = graph;
    // Setup getNodes function.
    // sourcer.getNodes = Drupal.behaviors.graphs_s_htmltable.getGetNodes(sourcer_id);
    sourcer.getGraph = Drupal.behaviors.graphs_s_htmltable.getGraph;
  },

  /**
   * Find the graph root if one.
   *
   * Returns the node that has no direct link with others (ie. other
   * nodes may be linked to this node while this node has no reciprocal link) or
   * which links are all of type 'parent_of'. However, such a node might be
   * discarded if an other node has a relationship link of type 'parent_of' to
   * this node.
   * If several nodes are found, if allow_multiple_roots is set to false, null
   * is returned and if allow_multiple_roots is set to true, an array of root
   * nodes is returned.
   *
   * @param string sourcer_id
   *   id of the sourcer.
   * @param bool allow_multiple_roots
   *   allow multiple valid roots found to be returned in an array.
   *
   * @return string
   *   the root node identifier or null if none found. If several roots are
   *   found and allow_multiple_roots is true, returns an array of root node
   *   identifiers.
   */
  getRoot: function (sourcer_id, allow_multiple_roots = false) {
    var sourcer = Drupal.settings.graphs.sourcers[sourcer_id];
    var nodes = sourcer.nodes;
    var root_id = null;
    var root_nodes = {};
    // Process all nodes to find ones without parents.
    for (var node_id in nodes) {
      if (nodes.hasOwnProperty(node_id)) {
        var node = nodes[node_id];
        // Check if node has no parents and has no other relationships than
        // children (hash initialized with 2 keys, 'child_of' and 'parent_of')
        // and has not been processed yet.
        if ((!node.relationships['child_of'].length)
            && (2 == Object.keys(node.relationships).length)
            && !(root_nodes.hasOwnProperty(node_id))) {
          root_nodes[node_id] = true;
        }
        else {
          // Not a root.
          root_nodes[node_id] = false;
        }

        // Now check node parental relationships.
        nodes[node_id].relationships['parent_of'].forEach(function (reciprocal_relationship) {
          root_nodes[reciprocal_relationship.target] = false;
        });
      }
    }

    // Process root candidates.
    for (var root_node_id in root_nodes) {
      if (root_nodes.hasOwnProperty(root_node_id)
          && root_nodes[root_node_id]) {
        // Check if we already got a root.
        if (null == root_id) {
          // No root yet, found one!
          root_id = root_node_id;
        }
        else if (allow_multiple_roots) {
          // Multiple roots allowed.
          // Is root_id an array?
          if (Array.isArray(root_id)){
            // Add current node as a root.
            root_id.push(root_node_id);
          }
          else {
            // Make it an array.
            root_id = [root_id, root_node_id];
          }
        }
        else {
          // Multiple roots found while only one requested, stop here.
          return null;
        }
      }
    }
    return root_id;
  },

  /**
   * Returns the function that provides nodes for the given sourcer (id).
   *
   * @param integer sourcer_id
   *   a sourcer id.
   *
   * @return function
   * Returns a function that returns the nodes linked to the given node by the
   * given relationship. If no relationship has been provided, returns all the
   * linked nodes. If no node has been specified, returns the root node and its
   * children.
   * Function parameters:
   * - node_id string: a node identifier or null to select graph root;
   * - depth int: the max depth of linked nodes from given node (default 1);
   * - rel_type mixed: relationship between the linked nodes and current node.
   *   (default set to "null" which means "any" type of relationship).
   *   The relationship should be read as: "node_id" "rel_type"
   *   "<returned node ids>".
   * - back_rel_type mixed: relationship between current node and the linked
   *   nodes. (default set to "null" which means no relationship).
   *   The relationship should be read as reciprocal relationship:
   *   "<returned node ids>" "rel_type" "node_id".
   * - skip_node_ids object: a hash of linked node id (hash keys) to ignore.
   */
  getGraph: function (runtime_id, sourcer_id, parameters) {
      var graph = Drupal.settings.graphs.graphs[runtime_id];
      var sourcer = Drupal.settings.graphs.sourcers[sourcer_id];

      // Process parameters.
      var skip_node_ids = {};
      if (null != parameters.skip_node_ids) {
        skip_node_ids = parameters.skip_node_ids;
      }
      
      var node, relationships = {}, node_ids = [];
      var subgraph = {
        'nodes': [],
        'links': []
      };

      // No depth, stop here.
      if (!parameters.depth || (0 >= parameters.depth)) {
        if (parameters.callback) {
          parameters.callback(subgraph);
        }
        return;
      }

      // Check if a node id has been specified.
      if (null == parameters.node_id) {
        // None, work from root.
        parameters.node_id = Drupal.behaviors.graphs_s_htmltable.getRoot(sourcer_id);
        if (parameters.node_id) {
          // Add root to the structure.
          subgraph.nodes.push(sourcer.nodes[parameters.node_id]);
        }
      }
      node = sourcer.nodes[parameters.node_id];

      // Make sure we found a node to work on.
      if (null == parameters.node_id || null == node) {
        console.log('graphs_s_htmltable: Failed to find node.' + (null != parameters.node_id ? ' id: ' + parameters.node_id : ''));
        if (parameters.callback) {
          parameters.callback(subgraph);
        }
        return;
      }

      // Check if a type of relationship has been specified.
      if (null != parameters.rel_types) {
        var rel_type_hash = {};
        // Transform rel_types into a hash object.
        if (Array.isArray(parameters.rel_types)) {
          parameters.rel_types.forEach(function (rel_type_val) {
            rel_type_hash[rel_type_val] = true;
          });
        }
        else if (typeof parameters.rel_types === 'string') {
          rel_type_hash[parameters.rel_types] = true;
        }
        else if (typeof parameters.rel_types !== 'object') {
          console.log('graphs_s_htmltable: invalid rel_types parameter!');
          if (parameters.callback) {
            parameters.callback(subgraph);
          }
          return;
        }

        // Loop on node relationships.
        for (var node_rel_type in node.relationships) {
          // Filter requested relationships.
          if (rel_type_hash.hasOwnProperty(node_rel_type)) {
            node.relationships[node_rel_type].forEach(function (relationship) {
              // We assume that relationship.source is always current node as it
              // has been set in Init() function.
              relationships[relationship.target] = relationship;
            });
          }
        }
      }
      else {
        // Merge all relationships.
        for (var node_rel_type in node.relationships) {
          node.relationships[node_rel_type].forEach(function (relationship) {
            relationships[relationship.target] = relationship;
          });
        }
      }

      // Current node can be ignored to avoid infinite loops.
      skip_node_ids[parameters.node_id] = true;
      // Here we got the list of directly-linked and filtered nodes in node_ids.
      // We will return the structured nodes in a graph object.
      for (linked_id in relationships) {
        // Skip nodes to ignore.
        if (!skip_node_ids.hasOwnProperty(linked_id)) {
          // Save linked node.
          subgraph.nodes.push(sourcer.nodes[linked_id]);
          // Save relationship.
          subgraph.links.push(relationships[linked_id]);
          // Add sub-levels.
          sourcer.getGraph(
            runtime_id,
            sourcer_id,
            {
              graph_id: graph.id,
              node_id: linked_id,
              depth: parameters.depth - 1,
              rel_types: parameters.rel_types,
              back_rel_types: parameters.back_rel_types,
              skip_node_ids: skip_node_ids,
              callback: function (sub_subgraph) {
                subgraph.nodes = subgraph.nodes.concat(sub_subgraph.nodes);
                subgraph.links = subgraph.links.concat(sub_subgraph.links);
              }
            }
          );
        }
      }

      if (parameters.callback) {
        parameters.callback(subgraph);
      }
  }
};

}(jQuery));
